#include <stdio.h>
#include <stdlib.h>
#include "info.h"
int main(int argc, char const *argv[]) {

appInfoDAta("Tipos de datos y direcciones de memoria","19/10/2014");

char letra='a';
unsigned char letraExtendida='a';

short enteroCorto=10;
int entero=30123;
unsigned int enteroLargo=2345898898;

float decimal=11.4f;
double decimalDoblePrec=12.9989d;

printf("Valor:%c direccion:%x, tamano:%d Byte (s)\n", letra,&letra,sizeof(letra));
printf("Valor:%c direccion:%x, tamano:%d Byte (s)\n", letraExtendida,&letraExtendida,sizeof(letraExtendida));

  system("pause");
  return 0;
}
